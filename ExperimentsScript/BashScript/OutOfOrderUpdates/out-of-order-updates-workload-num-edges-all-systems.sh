#!/bin/bash
####### clear results file #######

# HAL
./out-of-order-updates-workload-num-edges-hal.sh

# HAL-I
./out-of-order-updates-workload-num-edges-hal-no-dup-support.sh

# LiveGraph
./out-of-order-updates-workload-num-edges-livegraph.sh

